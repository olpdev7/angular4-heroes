import { Component } from '@angular/core';
import { HeroDetailComponent } from './hero-detail/hero-detail.component';

@Component({
  selector: 'my-app',
  templateUrl: './app.component.html',
  styleUrls: ['app.component.css'],
})
export class AppComponent {}